console.log("App starting");
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        var data = localStorage.getItem("LocalData");
        if (data) {
            data = JSON.parse(data);
            document.getElementById('urlinput').value = data.url;
        }
        else {
            data = {url: "http://test.ru?code="};
            data = JSON.stringify(data);
            localStorage.setItem("LocalData", data);
            document.getElementById('urlinput').value = data.url;
        }
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        console.log('Received Event: ' + id);
    }
};

app.initialize();

function scan()
{
    cordova.plugins.barcodeScanner.scan(
        function (result) {
            console.log("Scan complete");
            if(!result.cancelled)
            {
                //result.format == "QR_CODE"
                var value = result.text;
                console.log(value);
                alert("Done");
                sendData(value);
            }
        },
        function (error) {
            alert("Scanning failed: " + error);
        }
    );
}

function sendData(data) {
    var url = {url: document.getElementById('urlinput').value};
    url = JSON.stringify(url);
    localStorage.setItem("LocalData", url);
    console.log("send data");
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            console.log(xhttp.responseText);
            if (xhttp.responseText === "true") {
                document.getElementById("result").innerHTML = "ДОСТУП РАЗРЕШЕН";
            }
            else {
                document.getElementById("result").innerHTML = "ДОСТУП ЗАПРЕЩЕН";
            }
        }
        else {
            console.log("Error request");
            document.getElementById("result").innerHTML = data + "<br> ОШИБКА ЗАПРОСА";
        }
    };
    url = document.getElementById('urlinput').value;
    xhttp.open("GET", url + data, true);
    xhttp.send();
}